# simple-dockerfile-sast-iac-test

Demonstration of configuring GitLab SAST IaC scanning with custom kics queries.

This MR uses [SAST Customized Rulesets](https://docs.gitlab.com/ee/user/application_security/sast/index.html#customize-rulesets) to load
custom queries stored within https://gitlab.com/theoretick/custom-kics-queries in place of the packaged kics queries.

The PoC is based on one of our internal Sec Section development guidelines in which our containerized releases should rely on a `gitlab` user
rather than default `root` user, see https://about.gitlab.com/handbook/engineering/development/secure/#dockerfile.